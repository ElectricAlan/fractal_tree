import java.util.ArrayList;
import java.awt.*;
import javax.swing.*;

public class TreePanel extends JPanel
{
	private ArrayList<Tree> roots; //supports multiple roots

	// constructor, sets the background colour and 
	// saves a reference to the Tree.
	
    public TreePanel(ArrayList<Tree> roots)
    {
       setBackground(Color.BLACK);
       this.roots = roots;
       
    }

    public void paintComponent(Graphics g)
    {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		for(Tree t : roots)
			t.drawMe(g2d);
    }
}
