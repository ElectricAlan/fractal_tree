import java.awt.*;           // For Graphics, Color etc.
import java.awt.geom.*;
import java.util.*;

public class Tree
{	
    private double sX, sY, fX, fY;
    private double l, angle;
	private double rDist, gDist, bDist;
    private Tree treeA, treeB, treeC;
    private Color c; 
    private Line2D.Double line;
    private int depth;
    public static int offset = 0;
    public static int branchularity = 1;
    
    private Random x = new Random();
	
	double temp1, temp2, temp3;
	static double twoPiOn3 = (2.0/3.0)*Math.PI;
	
    //static state variables which define tree bahaviour
	public static double decay = 0.7;
	public static int threshold = 10;
    public static int colourMode = 8;
	public static boolean fixedAngle = false;
    public static boolean pulsate = true;

    public Tree(double x, double y, double theta, double length, int d, boolean root)
    {
    	line = new Line2D.Double(sX,sY,fX,fY);
    	angle = theta;	
    	depth = d;
       	sX = x;
       	sY = y;
       	l = length;
       
       fX = x + l*Math.cos(angle);
       fY = y + l*Math.sin(angle);
       
		if(depth < threshold)
		{
    		//YAY recursion!!!
			treeA = new Tree(fX, fY, getAngle('a'), l*0.7, depth+1,false);
       	    treeB = new Tree(fX, fY, getAngle('b'), l*0.7, depth+1,false);
       	    treeC = new Tree(fX, fY, getAngle('a'), l*0.7, depth+1,false);
        }
        
        //this tree is a root, therefore it will have a timer to itself
        if(root)
        {
        	//make a new timer n shit all up in this bitch
        }
    }
    
    public void changeThreshold()
    {
    	threshold--;
    	if(threshold == 2) threshold = 10;
    }
    
    public void changeColourMode()
    {
    	colourMode++;
    	if(colourMode == 9) colourMode = 0;
    }
    
    public void setColourMode(int i)
    {
    	colourMode = i;
    }
    
    public void toggleFixedAngle()
    {
    	fixedAngle = !fixedAngle;
    }
    
	public void setColor(Color colour)
	{
		this.c = colour;
	}
	
	//get colour using current colour scheme
    public void setCurrentColour(Tree t)
    {
    	switch(colourMode)
    	{
    		//random colour for each branch every frame
    		case 1:
    			if(x.nextInt(10) == 1)
    			{
    				t.setColor(new Color(x.nextInt(255),x.nextInt(255),x.nextInt(255)));
    			}
    			return;
    			
    		//length based colour scheme	
    		case 2:
    			if(l < 15)
    			{	
    				t.setColor(Color.red);
    				return;
    			}
    			if(l < 20)
    			{
    				t.setColor(Color.blue);
    				return;
    			}
    			if(l < 30)
    			{
    				t.setColor(Color.green);
    				return;
    			}
    			if(l < 50)
    			{
    				t.setColor(Color.yellow);
    				return;
    			}
    			if(l < 65)
    			{
    				t.setColor(Color.red);
    				return;
    			}
    			if(l < 80)
    			{
    				t.setColor(Color.magenta);
    				return;
    			}
    			if(l < 100)
    			{
    				t.setColor(Color.blue);
    				return;
    			}
    			if(l < 120)
    			{
    				t.setColor(Color.green);
    				return;
    			}
    			if(l < 150)
    			{
    				t.setColor(Color.red);
    				return;
    			}
    			if(l < 200)
    			{
    				t.setColor(Color.magenta);
    				return;
    			}
    			if(l < 300)
    			{
    				t.setColor(Color.cyan);
    				return;
    			}
    			
    			//default
    			t.setColor(Color.yellow);
    			return;
				
			//depth base colour scheme	
			case 3:
				if(depth == 0)
					t.setColor(Color.black);
				
				switch(depth%6 +1)
				{
					case 6:
						t.setColor(Color.red);
						return;
					case 5:
						t.setColor(Color.yellow);
						return;
					case 4:
						t.setColor(Color.green);
						return;
					case 3:
						t.setColor(Color.cyan);
						return;
					case 2:
						t.setColor(Color.blue);
						return;
					case 1:
						t.setColor(Color.magenta);
						return;
			
					default: t.setColor(Color.black);
				}
				return;
				
			//depth based colour with time variance
			case 4:
				if(depth == 0)
					t.setColor(Color.black);
				
				switch((depth + offset) %6 + 1 )
				{
					case 6:
						t.setColor(Color.red);
						return;
					case 5:
						t.setColor(Color.yellow);
						return;
					case 4:
						t.setColor(Color.green);
						return;
					case 3:
						t.setColor(Color.cyan);
						return;
					case 2:
						t.setColor(Color.blue);
						return;
					case 1:
						t.setColor(Color.magenta);
						return;
			
					default: t.setColor(Color.black);
				}
				return;
							
			//distance based colour scheme
			case 5:
				rDist = TreeFrame.red.distance(fX, fY); 
				gDist = TreeFrame.green.distance(fX, fY); 
				bDist = TreeFrame.blue.distance(fX, fY); 
				
				t.setColor(new Color((int)rDist%255,(int)gDist%255,(int)bDist%255));
				return;
			
			//distance based colour scheme with depth variance
			case 6:
				rDist = TreeFrame.red.distance(fX, fY); 
				gDist = TreeFrame.green.distance(fX, fY); 
				bDist = TreeFrame.blue.distance(fX, fY); 
				
				t.setColor(new Color((int)(rDist+20.0*depth)%255,(int)(gDist+20.0*depth)%255,(int)(bDist+20.0*depth)%255));
				return;
							
			//distance based colour scheme with time variance
			case 7:
				temp1 = Math.sin(TreeFrame.count/500.0);
				temp2 = Math.sin(twoPiOn3 + TreeFrame.count/500.0);
				temp3 = Math.sin(2*twoPiOn3 +TreeFrame.count/500.0);
				
				rDist = TreeFrame.red.distance(temp1*fX, temp1*fY); 
				gDist = TreeFrame.green.distance(temp2*fX, temp2*fY); 
				bDist = TreeFrame.blue.distance(temp3*fX, temp3*fY); 
				
				t.setColor(new Color((int)rDist%255,(int)gDist%255,(int)bDist%255));
				return;
			
			//distance based colour scheme with time and depth variance
			case 8:
				temp1 = Math.sin(TreeFrame.count/500.0);
				temp2 = Math.sin(twoPiOn3 + TreeFrame.count/500.0);
				temp3 = Math.sin(2*twoPiOn3 +TreeFrame.count/500.0);
				
				rDist = TreeFrame.red.distance(temp1*fX, temp1*fY); 
				gDist = TreeFrame.green.distance(temp2*fX, temp2*fY); 
				bDist = TreeFrame.blue.distance(temp3*fX, temp3*fY); 
				
				temp1 = (rDist+10.0*depth)%255;
				temp2 = (gDist+10.0*depth)%255;
				temp3 = (bDist+10.0*depth)%255;
				
				t.setColor(new Color((int)temp1,(int)temp2,(int)temp3));
				return;
				
			//plain white tree
    		default: t.setColor(Color.white);
    	}
    }
    
    public void update(double x, double y, double theta, double length)
    {
		angle = theta;
		l = length;  
		sX = x;
		sY = y;
       
		fX = x + l*Math.cos(angle);
		fY = y + l*Math.sin(angle);
    	
    	if(treeA!=null)
    	{
    		treeA.update(fX,fY, getAngle('a'), getLength('a'));
    	}
    	if(treeB!=null)
    	{
    		treeB.update(fX,fY, getAngle('b'), getLength('b'));
    	}
       	if(branchularity == 3 || (branchularity == 1 && depth%2 == 0) )
       	{
       		if(treeC!=null)
    		{
       	 	   treeC.update(fX,fY, getAngle('c'),getLength('c'));       	    
       		}
       	}
    }
    
    //returns current angle for each branch at current value of count
    public double getAngle(char id)
    {
    	switch(id)
    	{
    		case 'a':
    			return angle + (TreeFrame.count/800.0)*Math.PI + 0.5*depth*Math.sin(TreeFrame.count*0.05+2)/12;
    			
    		case 'b':
    			if(fixedAngle)
    			{
    				return TreeFrame.angle + getAngle('a') + (Math.PI * Math.sin(TreeFrame.count*0.001*Math.PI));
    			}
    			return angle + (TreeFrame.count/1500.0)*Math.PI + 0.5*depth*Math.sin(TreeFrame.count*0.05+2)/12;
    		
    		case 'c':
    			return angle + (TreeFrame.count/2000.0)*Math.PI + 0.5*depth*Math.sin(TreeFrame.count*0.05+2)/12;
    		default: return angle;
    	}
    }
    
    //returns current length of each branch at current value of count
    public double getLength(char id)
    {
    	if(!pulsate)
    	{
    		return l*decay;
    	}
    	
    	switch(id)
    	{
    		case 'a':
    			return l*(decay - Math.sin(TreeFrame.count*0.05+6)/20 + 0.5*depth*Math.sin(TreeFrame.count*0.05+2)/12);
    					
    		case 'b':
    			return l*(decay + Math.sin(TreeFrame.count*0.05)/20 - 0.5*depth*Math.cos(TreeFrame.count*0.05 + 1)/10);
    			
    		case 'c':
    			return l*(decay + Math.sin(TreeFrame.count*0.05 + 4)/10 + 0.5*depth*Math.cos(TreeFrame.count*0.05)/10);
    	}
    	return l*decay;
    }
       
    // draw the Tree object and its children in this method
    public void drawMe(Graphics2D g2d)
    {
		//draw children
		if(depth < threshold)
    	{
    	/*
    		for(Tree t : children)
    		{
    			t.drawMe(g2d);
    		}
    		
    		*/
    		if(treeA != null)
    		{
    			treeA.drawMe(g2d);
    		}
 	   		
 	   		if(treeB != null)
    		{
 	   			treeB.drawMe(g2d);
    		}
    		
    		if(treeC != null)
    		{
    			if(branchularity == 3 || (branchularity == 1 && depth%2 == 0) )
       	    	{
   					treeC.drawMe(g2d);
    			}
    		}
    	}
    	
    	//draw branch after chidren
    	if(TreeFrame.hiddenDepth < depth && !g2d.getColor().equals(Color.BLACK))
		{
			setCurrentColour(this);
    		g2d.setColor(c);
			line.setLine(sX,sY,fX,fY);
			g2d.draw(line);
	    }
    }  
}
