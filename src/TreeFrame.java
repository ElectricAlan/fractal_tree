import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;  
import java.awt.geom.*;
import java.util.*;

// displays window holding a Tree panel 

public class TreeFrame extends JFrame implements ActionListener, ChangeListener, ItemListener
{
	private JButton threshold, stepForward, stepBack, hideRoots, speed, playP, colourP, structP, fullScreenB;
	private JCheckBox pause, fixedAngle, pulsate, reverse, party;
	public JSlider speedSlider, angleSlider, colourSlider, branchularitySlider;
	public JPanel main, leftPanel, metaPanel, playPanel, colourPanel, structurePanel, prev;
	public JLabel colourLabel;
	public JMenuBar menuBar;
	public JMenu viewMenu;
	public JMenuItem fullScreenM, playM, colourM, structureM;
	
	
	///various state variables
    public static int count=0;
	public static double angle = Math.toRadians(90.0);
    public static int hiddenDepth = 0;
	public boolean animate = true;
	public boolean modified = false;
	public boolean reversed = false;
    public static int WIDTH = 800;
    public static int HEIGHT = 450;
	public int sleepTime = 20; 
	public boolean partyHard, fullScreen = false;
	
	int x,y;
	Dimension tempSize;
	public boolean fixed = false;
    private Random rand = new Random();	
	public static Point red, green, blue;
	
	public ArrayList<Tree> roots;
	
    Tree myTree;
    TreePanel tp;
    
    // constructor 
    public TreeFrame()
    {
    	super("Fractal Tree");

   	   	this.setMinimumSize(new Dimension(800,450));

	   	addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e)
			{
			    	System.exit(0);
			}
		});
		
		//initialize Tree and assorted panels
		roots = new ArrayList<Tree>();
		myTree = new Tree(WIDTH/2,0,Math.PI/2.0,HEIGHT/3.0,1,true);
		roots.add(myTree);
		//roots.add(new Tree(0,2*(HEIGHT/3.0),0,WIDTH/4.0,1));
		tp = new TreePanel(roots);

		
		menuBar = new JMenuBar();
		viewMenu = new JMenu("view");
		
		playM = new JMenuItem("play menu");
		playM .setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P,2));
		playM.addActionListener(this);
		viewMenu.add(playM);
		
		colourM = new JMenuItem("colour menu");
		colourM .setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,2));
		colourM.addActionListener(this);
		viewMenu.add(colourM);
		
		structureM = new JMenuItem("structure menu");
		structureM .setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,2));
		structureM.addActionListener(this);
		viewMenu.add(structureM);

		viewMenu.addSeparator();

		fullScreenM = new JMenuItem("full screen");
		fullScreenM.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F,2));
		fullScreenM.addActionListener(this);
		viewMenu.add(fullScreenM);
		
		menuBar.add(viewMenu);
		
		this.setJMenuBar(menuBar);
		
		main = new JPanel();
		main.setLayout(new BorderLayout());
		
		playPanel = new JPanel();
		playPanel.setLayout(new BoxLayout(playPanel, BoxLayout.X_AXIS));
		playPanel.setVisible(false);
		
		colourPanel = new JPanel();
		colourPanel.setLayout(new BoxLayout(colourPanel, BoxLayout.X_AXIS));
		colourPanel.setVisible(false);
		
		structurePanel = new JPanel();
		structurePanel.setLayout(new BoxLayout(structurePanel, BoxLayout.X_AXIS));
		structurePanel.setVisible(false);
		
		metaPanel = new JPanel();
		metaPanel.add(playPanel);
		metaPanel.add(colourPanel);
		metaPanel.add(structurePanel);

		//leftPanel
		playP = new JButton("play");
		playP.addActionListener(this);
		
		colourP = new JButton("colour");
		colourP.addActionListener(this);
		
		structP = new JButton("structure");
		structP.addActionListener(this);
		
		fullScreenB = new JButton("full screen");
		fullScreenB.addActionListener(this);
		
		leftPanel = new JPanel();
		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
		leftPanel.add(playP);
		leftPanel.add(colourP);
		leftPanel.add(structP);
		leftPanel.add(fullScreenB);
		
		//structurePanel
		branchularitySlider = new JSlider(1,3,2);
		branchularitySlider.setSize(new Dimension(30,30));
		branchularitySlider.addChangeListener(this);
		branchularitySlider.setSnapToTicks(true);
		branchularitySlider.setPaintTicks(true);
		branchularitySlider.setMajorTickSpacing(1);
		
		threshold = new JButton("depth");
		threshold.addActionListener(this);
		
		hideRoots = new JButton("hide roots " + hiddenDepth);
		hideRoots.addActionListener(this);
		
		fixedAngle = new JCheckBox("fixed angle");
		fixedAngle.addItemListener(this);
		
		pulsate = new JCheckBox("pulsate",true);
		pulsate.addItemListener(this);
		
		angleSlider = new JSlider(0,180,90);
		angleSlider.addChangeListener(this);
		angleSlider.setEnabled(false);
		
		structurePanel.add(branchularitySlider);
		structurePanel.add(Box.createRigidArea(new Dimension(20,0)));
		structurePanel.add(threshold);
		structurePanel.add(Box.createRigidArea(new Dimension(20,0)));
		structurePanel.add(hideRoots);
		structurePanel.add(Box.createRigidArea(new Dimension(20,0)));
		structurePanel.add(pulsate);
		structurePanel.add(Box.createRigidArea(new Dimension(20,0)));
		structurePanel.add(fixedAngle);
		structurePanel.add(Box.createRigidArea(new Dimension(20,0)));
		//structurePanel.add(angleSlider);
		
		//play panel
		stepForward = new JButton("step");
		stepForward.addActionListener(this);
		stepForward.setEnabled(!animate);
		
		stepBack = new JButton("back");
		stepBack.addActionListener(this);
		stepBack.setEnabled(!animate);

		pause = new JCheckBox("pause");
		pause.addItemListener(this);
		
		reverse = new JCheckBox("reverse");
		reverse.addItemListener(this);
		
		speedSlider = new JSlider(0,40,20);
		speedSlider.addChangeListener(this);
		
		playPanel.add(pause);
		playPanel.add(Box.createRigidArea(new Dimension(20,0)));
		playPanel.add(reverse);
		playPanel.add(Box.createRigidArea(new Dimension(20,0)));
		playPanel.add(stepForward);
		playPanel.add(Box.createRigidArea(new Dimension(20,0)));
		playPanel.add(stepBack);
		playPanel.add(Box.createRigidArea(new Dimension(20,0)));
		playPanel.add(new JLabel("speed"));
		playPanel.add(Box.createRigidArea(new Dimension(20,0)));
		playPanel.add(speedSlider);

		//set up colour panel
		colourSlider = new JSlider(0,8,8);
		colourSlider.addChangeListener(this);
		colourSlider.setSnapToTicks(true);
		colourSlider.setPaintTicks(true);
		colourSlider.setMajorTickSpacing(1);

		colourLabel = new JLabel("colour " + Tree.colourMode);
		
		party = new JCheckBox("PARTY HARD");
		party.addItemListener(this);

		colourPanel.add(colourLabel);
		colourPanel.add(Box.createRigidArea(new Dimension(20,0)));
		colourPanel.add(colourSlider);
		colourPanel.add(Box.createRigidArea(new Dimension(20,0)));
		colourPanel.add(party);
		
		//main panel
		main.add(tp, BorderLayout.CENTER);
		main.add(leftPanel,BorderLayout.WEST);
		main.add(metaPanel,BorderLayout.SOUTH);
		setContentPane(main);
				
		setVisible(true);
		addView(structurePanel);
		
		//initiaize points required for some colour schemes
		tempSize = tp.getSize();
		x = tempSize.width;
		y = tempSize.height;
	
		red = new Point(x/4,y/3);
		green = new Point(x/2,2*y/3);
		blue = new Point(3*x/4,y/3);
		
		//main program loop
		while(true)
		{	
			try
			{
				if(Tree.branchularity == 3)
				{
					Thread.sleep(sleepTime/2);
				}
				else
				{
					Thread.sleep(sleepTime);
				}
			}
			catch(Exception e)
			{
				//nothing
			}
			
			if(animate)
			{
				if(reversed)
				{
					count--;
				}
				else
				{
					count++;
				}
				
				if(count%50 == 0)
				{
					Tree.offset = (Tree.offset + 1) % 6;
				}	
			}
			
			if(partyHard)
			{
				//tp.setBackground(new Color(rand.nextInt(255),rand.nextInt(255),rand.nextInt(255)));
			}
			
			if(animate || modified)
			{
				tempSize = tp.getSize();
				x = tempSize.width;
				y = tempSize.height;
	
				red.move(x/4,y/3);
				green.move(x/2,2*y/3);
				blue.move(3*x/4,y/3);
		
				myTree.update(x/2.0,0,Math.PI/2.0,y/3.0);
				modified = false;
			}
			tp.repaint();
		}
    }
    
	public void addView(JPanel panel)
	{
		if(prev != null)
			prev.setVisible(false);
		prev = panel;
		prev.setVisible(true);
	}

   	public void actionPerformed(ActionEvent event)
	{
		modified = true;
	
		if(event.getSource() == threshold)
		{
			myTree.changeThreshold();
			return;
		}
		
		if(event.getSource() == stepForward)
		{
			count+=5;
			return;
		}
		
		if(event.getSource() == stepBack)
		{
			count-=5;
			return;
		}	
		if(event.getSource() == hideRoots)
		{
			hiddenDepth++;
			
			if(hiddenDepth > 7)
				hiddenDepth = 0;

			hideRoots.setText("hide roots " + hiddenDepth);				
			return;
		}	
		//user can change panel view with button or file menu
		if(event.getSource() == playP || event.getSource() == playM)
		{
			addView(playPanel);
			return;
		}
		
		
		if(event.getSource() == colourP || event.getSource() == colourM)
		{
			addView(colourPanel);
			return;
		}
		
		if(event.getSource() == structP || event.getSource() == structureM)
		{
			addView(structurePanel);
			return;
		}
		
		if(event.getSource() == fullScreenB || event.getSource() == fullScreenM)
		{
			
			metaPanel.setVisible(fullScreen);
			leftPanel.setVisible(fullScreen);
			fullScreen = !fullScreen;
		}
	}
	
	public void itemStateChanged(ItemEvent event)
	{
		if(event.getSource() == pause)
		{
			animate = !animate;
			stepForward.setEnabled(!animate);
			stepBack.setEnabled(!animate);
			return;
		}
		
		if(event.getSource() == fixedAngle)
		{
			if(Tree.branchularity != 3)
			{
				myTree.toggleFixedAngle();
				fixed = !fixed;
				angleSlider.setEnabled(!angleSlider.isEnabled());
			}
			return;
		}
		
		if(event.getSource() == pulsate)
		{
			Tree.pulsate = !Tree.pulsate;
			return;
		}
		
		if(event.getSource() == reverse)
		{
			reversed = !reversed;
		}
		
		if(event.getSource() == party)
		{
			if(partyHard)
				tp.setBackground(Color.BLACK);
			partyHard = ! partyHard;
			
		}
	}
	
	public void stateChanged(ChangeEvent event) 
	{
		modified = true;
		if(event.getSource() == speedSlider)
		{
			sleepTime = 40 - speedSlider.getValue();
			return;
		}
		
		if(event.getSource() == angleSlider)
		{
			angle = Math.toRadians(angleSlider.getValue());
			return;
		}
		
		if(event.getSource() == colourSlider)
		{
			myTree.setColourMode(colourSlider.getValue());
			colourLabel.setText("colour " + Tree.colourMode);
			return;
		}
		
		if(event.getSource() == branchularitySlider)
		{
			Tree.branchularity = branchularitySlider.getValue();
		}
	}
	
    public static void main(String [] args)
    {
		new TreeFrame();
    }
}
