Fractal Tree program, by Alan Carey

Running the program
	The simplest way is to run the jar file, to do this u will need to have java installed, which you can get here http://www.java.com/en/download/manual.jsp
	If you wish you can compile and run it yourself in a terminal by typing "ant run"
	
Description
	This tree is initialy a simple binary tree, where each branch has two children and and throughout the whole tree they have the same relative angle from the parent. A threshold value exists to stop branches under a certain length from sprouting children. There is also the functionality to draw a trinary tree, where each branch has three children instread of two.
	There are 3 buttons on the left of the panel that change what panel is currently viewed at the bottom. They swap between Play, Colour and Structure panels, these panels contain several buttons, checkboxes and sliders to manipulate the behavior of the tree, they are as follows;

	Buttons	
		Depth; alters depth of tree, if you're into that sort of thing
		Branches; toggles between binary and trinary modes, and displays number of branches
		Hide Roots; incrementally hides roots of the tree, and displays current hidden depth
		Step/Back; alters angle position when animation is paused, see Pause checkbox
	
	Checkboxes	
		Fixed Angle; not available in trinary mode, fixes the angle between children, can be adjusted using the angle slider
		Pulsate; varies length of children in a cyclic manner
		Reverse; reverses dipection of tree rotation
		Pause; pauses/unpauses animation
		PARTY HARD!; induces seziures
	
	Sliders
		Speed; varies animation speed
		Angle; see Fixed Angle checkbox
		Colour; selects different colour schemes, also displays the number of the current colour mode
	
Colour schemes
	0. White
	
	1. Random colour, each branch has around a one in ten chance of modification each animation step
	
	2. Length based colour
	
	3. Depth based colour
	
	4. Depth based colour with time variance
	
	5. spatial colour
	
	6. spatial colour with depth dependancy
	
	7. spatial colour with time variance
	
	8. spatial colour with time variance and depth depandancy
	
